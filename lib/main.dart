import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/rendering.dart';

List<List<double>> dotsList = [
  [0.0, 0.0]
];

void main() {
  runApp(TouchTest());
}

class TouchTest extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Touch Test',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Material(
        child: Center(child: TouchControl()),
      ),
    );
  }
}

class TouchControl extends StatefulWidget {
  final double xPos;
  final double yPos;
  final ValueChanged<Offset> onChanged;

  const TouchControl({Key key, this.onChanged, this.xPos: 0.0, this.yPos: 0.0}) : super(key: key);

  @override
  TouchControlState createState() => new TouchControlState();
}

class TouchControlState extends State<TouchControl> {
  double xPos = 0.0;
  double yPos = 0.0;
  int posCounter = 0;

  void onChanged(Offset offset) {
    final RenderBox referenceBox = context.findRenderObject();
    Offset position = referenceBox.globalToLocal(offset);
    if (widget.onChanged != null) widget.onChanged(position);

    setState(() {
      xPos = position.dx;
      yPos = position.dy;
    });
  }

  void _handlePanStart(DragStartDetails details) {
    posCounter++;
    onChanged(details.globalPosition);
    List<double> posTemp = [];
    posTemp.add(xPos);
    posTemp.add(yPos);
    dotsList.add(posTemp);
    //print(dotsList);
  }

  void _handlePanUpdate(DragUpdateDetails details) {
    onChanged(details.globalPosition);
  }

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints.expand(),
      child: GestureDetector(
        onPanStart: _handlePanStart,
        child: CustomPaint(
          size: Size(xPos, yPos),
          painter: TouchControlPainter(xPos, yPos),
        ),
      ),
    );
  }
}

class TouchControlPainter extends CustomPainter {
  static const markerRadius = 10.0;
  final double xPos;
  final double yPos;

  TouchControlPainter(this.xPos, this.yPos);

  @override
  void paint(Canvas canvas, Size size) {
    for (int i = 0; i < dotsList.length; i++) {
      final paintCircle = Paint()
        ..color = Colors.redAccent
        ..style = PaintingStyle.fill;

      final paintLane = Paint()
        ..color = Colors.red
        ..style = PaintingStyle.fill;

      if (i > 1) {
        canvas.drawLine(Offset(dotsList[i - 1][0], dotsList[i - 1][1]), Offset(dotsList[i][0], dotsList[i][1]), paintLane);
      }

      canvas.drawCircle(Offset(dotsList[i][0], dotsList[i][1]), markerRadius, paintCircle);
    }
    print('fine');
  }

  @override
  bool shouldRepaint(TouchControlPainter old) => false;
}
